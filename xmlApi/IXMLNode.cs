﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xmlApi {
	interface IXMLNode {
		IXMLNode loadXML(string path);
		IXMLNode setName(string name);
		IXMLNode setValue(string value);
		IXMLNode setType(NodeType type);
		IXMLNode addChild(IXMLNode node);
		IXMLNode addAttribute(string name, string value);
		IXMLNode setParent(IXMLNode parent);

		NodeType getType();
		string getName();
		string getValue();
		string getAttribute(string name);
		IXMLNode getParent();
		List<IXMLNode> getChildren();
		IXMLNode getNodeFromRelativeRef(string nodeRef);
	}
}