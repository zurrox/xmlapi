﻿using System.Collections.Generic;
using System;
using System.IO;
using System.Xml;

namespace xmlApi {
    class XMLNode {
        private Dictionary<string, string> attributes= new Dictionary<string,string>();
        private string name, value;
        private List<XMLNode> children = new List<XMLNode>();
        private XMLNode parent;
        private NodeType type;

        public XMLNode(string name, NodeType type, params object[] args) {

            if (type != NodeType.ROOT) {
                if (args.Length != 1) throw new ArgumentException("The NodeType: " + type.ToString() + " requires only one extra argument.");
                else parent = (XMLNode) args[0];
            }
        }

        public XMLNode() { } //Blank node - For initialization

        public static XMLNode loadXML(string path) {
            XMLNode outNode = null;
            if (File.Exists(path) && path.EndsWith(".xml", StringComparison.OrdinalIgnoreCase)) {
                string xmlString = File.ReadAllText(path);
                XmlReader reader = XmlReader.Create(new StringReader(xmlString));
                while (reader.Read()) {
                    if (!string.IsNullOrEmpty(reader.Name) || !string.IsNullOrEmpty(reader.Value)) {
						while (reader.NodeType == XmlNodeType.XmlDeclaration || reader.NodeType == XmlNodeType.EndElement) reader.Read();
                        if(outNode == null && !string.IsNullOrEmpty(reader.Name)) { //Initial Node
							//Can check reader.NodeType to make sure I'm not looking at EndElements(<\node>) - Node Types = Element, Attribute
							outNode = new XMLNode(reader.Name, NodeType.ROOT); //Initial non xml declaration node will always be root
						}else{ //Not root
							XMLNode child = new XMLNode();
							child.setParent(outNode);
							if (reader.Name != null) child.setName(reader.Name);
							if (reader.Value != null) child.setValue(reader.Value);
						}
                    }
                }
            }
            return outNode;
        }

		public static XMLNode loadNewXML(string path) {
			XMLNode nodeRoot = null;
			if (File.Exists(path) && path.EndsWith(".xml", StringComparison.OrdinalIgnoreCase)) {
				XmlReader reader = XmlReader.Create(new StringReader(File.ReadAllText(path)));
				List<string> openNodes = new List<string>(); //May need to store nodes and check node.getName() to add parents & children to
				while (!reader.EOF) {
					reader.Read();
					if (reader.NodeType != XmlNodeType.XmlDeclaration && reader.NodeType != XmlNodeType.Comment && 
						(!string.IsNullOrEmpty(reader.Name) || !string.IsNullOrEmpty(reader.Value)) && reader.NodeType != XmlNodeType.Whitespace) { //Don't need to read this, so can skip
						if(nodeRoot == null) nodeRoot = new XMLNode();
						Console.WriteLine("NodeType: " + reader.NodeType.ToString());
						if (!string.IsNullOrEmpty(reader.Name)) {
							if(nodeRoot.getName() == null) nodeRoot.setName(reader.Name);
							if (reader.NodeType == XmlNodeType.Element) { //Open Node Tag
									openNodes.Add(reader.Name);
									Console.WriteLine("<" + reader.Name + ">");
							} else if (reader.NodeType == XmlNodeType.EndElement) {
								if (openNodes.Contains(reader.Name)) {
									openNodes.Reverse(); //Removes last node
									Console.WriteLine("PRE - </" + openNodes[0] + ">");
									if (openNodes[0].Equals(reader.Name, StringComparison.OrdinalIgnoreCase)) { //For <test/> this must be false
										Console.WriteLine("POST - </" + openNodes[0] + ">"); 
										openNodes.RemoveAt(0);
									} //else could be - <test/>
									openNodes.Reverse();
								} else throw new XmlException("A tag with the name " + reader.Name + " has not been opened.");
							}
						}
					}
				}
				if (openNodes.ToArray().Length > 0) return null;
			}

			return nodeRoot;
		}

		public XMLNode setValue(string val) {
			this.value = val;
			return this;
		}

		public string getValue() {
			return value;
		}

        public string getName() {
            return name;
        }

        public XMLNode setType(NodeType type) {
            this.type = type;
            return this;
        }

        public XMLNode setName(string name) {
            this.name = name;
            return this;
        }

        public XMLNode setParent(XMLNode parent) {
			if (getParent().getChildren().Contains(this)) getParent().getChildren().Remove(this);
            this.parent = parent;
            return this;
        }

		public XMLNode addChild(XMLNode child) {
			if (!getChildren().Contains(child)) children.Add(child);
			child.setParent(this);
			return this;
		}

        public XMLNode getChild(string name) {
            foreach (XMLNode node in children) {
                if (node.getName().Equals(name, StringComparison.OrdinalIgnoreCase)) {
                    return node;
                }
            }
            return null;
        }

        public XMLNode getNodeFromRelativeRef(string relRef) { //relRef = controls.keyboard.controlSet
            string[] nodeNames = relRef.Split('.');
            XMLNode currentNode = this;
            foreach(string s in nodeNames) {
                XMLNode child = currentNode.getChild(s);
                if (child != null) currentNode = child;
                else return null;
            }

            return currentNode;
        }

        public XMLNode addAttribute(string name, string value) {
            attributes.Add(name, value);
            return this;
        }

        public List<XMLNode> getChildren() {
            return children;
        }

        public XMLNode getParent() {
            return parent;
        }

        public string getAttribute(string name) {
            string val = null;
            if (attributes.ContainsKey(name)) {
                attributes.TryGetValue(name, out val);
            }
            return val;
        }
    }

    public enum NodeType {
        ROOT,
        ELEMENT,
        SUBTREE
    }
}